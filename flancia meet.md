# flancia meet
- a [[space]].
	- #go https://meet.jit.si/flancia-meet
	- see also older node: [[flancia meet notes]]
- a [[meeting]].
  - [[recurring]], just show up if interested :) I attend often but not always.
	- when: [[Saturdays]] at [[10AM UTC]] by default.
