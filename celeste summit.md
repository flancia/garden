- an [[event]].
  - [[streaming]] [[working in public]]
  - #go https://meet.jit.si/celeste-summit

## [[2023-02-04]]

- [[timeline]]
- [[open assistant]]
- [[sorting memories]]
- [[agora chapter]]
- [[typescript]] and [[python]]
- https://on-exactitude-in.science/
